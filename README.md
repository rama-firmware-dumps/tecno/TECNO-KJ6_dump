## sys_tssi_64_armv82_tecno-user 13 TP1A.220624.014 603576 release-keys
- Transsion Name: TECNO SPARK 20 Pro
- TranOS Version: hios13.5.0
- Manufacturer: tecno
- Platform: mt6789
- Codename: TECNO-KJ6
- Brand: TECNO
- Flavor: sys_tssi_64_armv82_tecno-user
- Release Version: 13
- Kernel Version: 5.10.198
- Security Patch: 2024-05-05
- Id: TP1A.220624.014
- Incremental: 240509V1673
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: 480
- Fingerprint: TECNO/KJ6-OP/TECNO-KJ6:13/TP1A.220624.014/240509V1673:user/release-keys
- Branch: sys_tssi_64_armv82_tecno-user-13-TP1A.220624.014-603576-release-keys
- Repo: TECNO/TECNO-KJ6_dump
